<?php

function arreglo($size=10){

	for($i = 0; $i < $size; $i++){
		$arr[$i] = rand(0,20);
	}

	return $arr;
}

function arregloNeg($size=10){

	for($i = 0; $i < $size; $i++){
		$arr[$i] = rand(-20,20);
	}

	return $arr;
}

function p1($arr){
	$p = 0;
	for($i=0;$i<count($arr);$i++){
		$p += $arr[$i];	
	}
	return ($p/count($arr));
}

function p2($arr){
	sort($arr);
	$c = count($arr);
	if($c%2 == 0){
		$m=($arr[$c/2]+$arr[($c/2)-1])/2;
	}else{
		$m = $arr[($c-1)/2];
	}
	return $m;
} 

if(isset($_POST['p1_submit'])){

	$numP = 1;
	$p1_arr = arreglo($_POST["p1_range"]);
	$stringHtml = '
		<div class="input-field col s12">
	      <input disabled type="text" value="'.implode(', ',$p1_arr).'">
	      <label for="disabled">Arreglo</label>
	    </div>
	    <div class="input-field col s12">
	      <input disabled id="p1_prom" type="text" value="'.p1($p1_arr).'">
	      <label for="p1_prom">Promedio</label>
	    </div> 
	    ';	
	
	include("respuesta.html");
} else if(isset($_POST['p2_submit'])){

	$numP = 2;
	$p2_arr = arreglo($_POST["p2_range"]);
	$stringHtml = '
		<div class="input-field col s12">
	      <input disabled type="text" value="'.implode(', ',$p2_arr).'">
	      <label for="disabled">Arreglo</label>
	    </div>
	    <div class="input-field col s12">
	      <input disabled id="p2_med" type="text" value="'.p2($p2_arr).'">
	      <label for="p2_med">Mediana</label>
	    </div> 
	    ';	
	
	include("respuesta.html");
} else if(isset($_POST['p3_submit'])){

	$numP = 3;
	$p3_arr = arreglo($_POST["p3_range"]);
	$p3_arr2 = $p3_arr;
	sort($p3_arr2);
	$p3_arr3 = array_reverse($p3_arr2);
	$stringHtml = '
		<div class="input-field col s12">
	      <input disabled type="text" value="'.implode(', ',$p3_arr).'">
	      <label for="disabled">Arreglo</label>
	    </div>
	    <ul class="collection">
	   	  <span class="title">Promedio</span>
	      <li class="collection-item">'.p1($p3_arr).'</li>
	      <span class="title">Mediana</span>
	      <li class="collection-item">'.p2($p3_arr).'</li>
	      <span class="title">Sort</span>
	      <li class="collection-item">'.implode(', ',$p3_arr2).'</li>
	      <span class="title">Sort Reverse</span>
	      <li class="collection-item">'.implode(', ',$p3_arr3).'</li>
	    </ul>
	    ';	
	
	include("respuesta.html");
} else if(isset($_POST['p4_submit'])){

	$numP = 4;
	$tam = $_POST["p4_range"];
	$s = '';
	for ($i=1; $i <= $tam; $i++) { 
		$s.='<tr><td>'.$i.'</td><td>'.($i*$i).'</td><td>'.($i*$i*$i).'</td></tr>';
	}


	$stringHtml = '
	<div class="container">
      <table>
        <thead>
          <tr>
              <th >Numero</th>
              <th >Cuadrado</th>
              <th >Cubo</th>
          </tr>
        </thead>

        <tbody>'.$s.'
        </tbody>
      </table>
      </div>';	
	
	include("respuesta.html");
} else if(isset($_POST['p5_submit'])){

	$numP = 5;
	$dados = $_POST["p5_dados"];
	$r = $_POST["p5_range"];
	$s = '';
	for($i=0; $i<= $dados; $i++){
		$s.="<tr><td>".rand(1,$r)."</td></tr>";
	}

	$stringHtml = '
	<div class="container">
      <table>
        <thead>
          <tr>
              <th >Dados</th>
          </tr>
        </thead>
        <tbody>'.$s.'
        </tbody>
      </table></div>';
	
	include("respuesta.html");
}


?>