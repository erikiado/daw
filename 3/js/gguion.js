var nombre = prompt("Como te llamas?");

document.write("<h2 id='saludo'>Hola   <div id='nombre'>"+nombre+"</div></h2>");

function p1(){
	var s = "<table><thead><tr><th>i</th><th>Cuadrado</th><th>Cubo</th></tr></thead><tbody>";
	var num = Number(prompt("Hasta que numero llegara la tabla?"));

	for (var i = 1; i <= num; i++) {
		s += "<tr><td>"+i+"</td><td>"+(i*i)+"</td><td>"+(i*i*i)+"</td></tr>";
	};

	s += "</tbody></table>";
	document.getElementById("p1").innerHTML = s;
};

function p2(){
	var a = Math.floor(Math.random()*1000)%200 - 100;
	var b = Math.floor(Math.random()*1000)%400 - 200;
	var inicio = new Date();
	var res = Number(prompt(""+a+" + "+b+" ="));
	var fin = new Date();
	var correcto = (res == (a+b));
	var seg = fin.getSeconds() - inicio.getSeconds();
	if(seg < 0) 
		seg = 60 + seg;
	var s = ("Tiempo: "+seg+" segundos<br>");
	(correcto)?
			s += "Bien!<br>"+a+" + "+b+" = "+(a+b):
			s += a+" + "+b+" = "+(a+b)+"<br>Tu respuesta: "+ res;	
	document.getElementById("p2").innerHTML = s;
}

function p3(arr){
	var a,b,c;//negativo,zero y positivo
	a = b = c = 0;
	for (var i = arr.length - 1; i >= 0; i--) {
		var x = arr[i];
		if(x<0) a++;
		else if(x == 0) b++;
		else c++; 
	};
	var s = "";
	for (var i = 0; i < arr.length; i++) {
		if (i<arr.length-1){
			s+=arr[i] + ", ";
		} else{
			s+=arr[i] + ".";
		};
	};
	s+="<br>Negativos: "+a+"<br>Ceros: "+b+"<br>Positivos: "+c;
	document.getElementById("p3").innerHTML = s;
}

function arregloRandom(tam){
	var arr = new Array(Number(tam));
	for (var i = arr.length - 1; i >= 0; i--) {
		arr[i] = (Math.floor(Math.random()*200))-100;
	};
	return arr;
}

function p4(mat){
	var s = "<table><tbody>";
	for (var i = mat.length - 1; i >= 0; i--) {
		s += mat[i]+" = "+promedio(mat[i]).toFixed(2)+"<br>";
	};
	s += "</tbody></table>";
	document.getElementById("p4").innerHTML = s;
}

function matriz(tam){
	mat = new Array(Number(tam));
	for (var i = mat.length - 1; i >= 0; i--) {
		mat[i] = arregloRandom(tam);
	};
	return mat;
}

function promedio(arr){
	var p = 0;
	for (var i = arr.length - 1; i >= 0; i--) {
		p+=arr[i];
	};
	return p/arr.length;
}

function p5(num){
	num = num.split("").reverse().join("");
	document.getElementById("p5num").value = num;
}

function Dado(lInf,lSup){
	this.inf = lInf;
	this.sup = lSup;
	this.tirar = function tirar() {
					var r = (Math.floor(Math.random()*this.sup) + Number(this.inf));
					return r;	
				}
}

function p6(){
	var dado = new Dado(document.getElementById("p6inf").value,document.getElementById("p6sup").value);
	var s = "<table><tbody>";
	for (var i = 0; i < document.getElementById("p6dado").value; i++) {
		s +="<tr><td>"+dado.tirar()+"</td></tr>";
	}
	s+="</tbody></table>";
	document.getElementById("p6").innerHTML = s;
}

