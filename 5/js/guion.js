var nombre = prompt("Como te llamas?");

document.write("<h2 id='saludo'>Hola   <div id='nombre'>"+nombre+"</div></h2>");

function p1(){
	var s = "<table><thead><tr><th>i</th><th>Cuadrado</th><th>Cubo</th></tr></thead><tbody>";
	var num = Number(prompt("Hasta que numero llegara la tabla?"));

	for (var i = 1; i <= num; i++) {
		s += "<tr><td>"+i+"</td><td>"+(i*i)+"</td><td>"+(i*i*i)+"</td></tr>";
	};

	s += "</tbody></table>";
	document.getElementById("p1").innerHTML = s;
};

function p2(){
	var a = Math.floor(Math.random()*1000)%200 - 100;
	var b = Math.floor(Math.random()*1000)%400 - 200;
	var inicio = new Date();
	var res = Number(prompt(""+a+" + "+b+" ="));
	var fin = new Date();
	var correcto = (res == (a+b));
	var seg = fin.getSeconds() - inicio.getSeconds();
	if(seg < 0) 
		seg = 60 + seg;
	var s = ("Tiempo: "+seg+" segundos<br>");
	(correcto)?
			s += "Bien!<br>"+a+" + "+b+" = "+(a+b):
			s += a+" + "+b+" = "+(a+b)+"<br>Tu respuesta: "+ res;	
	document.getElementById("p2").innerHTML = s;
}

function p3(arr){
	var a,b,c;//negativo,zero y positivo
	a = b = c = 0;
	for (var i = arr.length - 1; i >= 0; i--) {
		var x = arr[i];
		if(x<0) a++;
		else if(x == 0) b++;
		else c++; 
	};
	var s = "";
	for (var i = 0; i < arr.length; i++) {
		if (i<arr.length-1){
			s+=arr[i] + ", ";
		} else{
			s+=arr[i] + ".";
		};
	};
	s+="<br>Negativos: "+a+"<br>Ceros: "+b+"<br>Positivos: "+c;
	document.getElementById("p3").innerHTML = s;
}

function arregloRandom(tam){
	var arr = new Array(Number(tam));
	for (var i = arr.length - 1; i >= 0; i--) {
		arr[i] = (Math.floor(Math.random()*200))-100;
	};
	return arr;
}

function p4(mat){
	var s = "<table><tbody>";
	for (var i = mat.length - 1; i >= 0; i--) {
		s += mat[i]+" = "+promedio(mat[i]).toFixed(2)+"<br>";
	};
	s += "</tbody></table>";
	document.getElementById("p4").innerHTML = s;
}

function matriz(tam){
	mat = new Array(Number(tam));
	for (var i = mat.length - 1; i >= 0; i--) {
		mat[i] = arregloRandom(tam);
	};
	return mat;
}

function promedio(arr){
	var p = 0;
	for (var i = arr.length - 1; i >= 0; i--) {
		p+=arr[i];
	};
	return p/arr.length;
}

function p5(num){
	num = num.split("").reverse().join("");
	document.getElementById("p5num").value = num;
}

function Dado(lInf,lSup){
	this.inf = lInf;
	this.sup = lSup;
	this.tirar = function tirar() {
					var r = (Math.floor(Math.random()*this.sup) + Number(this.inf));
					return r;	
				}
}

function p6(){
	var dado = new Dado(document.getElementById("p6inf").value,document.getElementById("p6sup").value);
	var s = "<table><tbody>";
	for (var i = 0; i < document.getElementById("p6dado").value; i++) {
		s +="<tr><td>"+dado.tirar()+"</td></tr>";
	}
	s+="</tbody></table>";
	document.getElementById("p6").innerHTML = s;
}

function validar(){
	var s = "";
	if (document.getElementById("pwd1").value !== ""){
		s = "<br>Password: "+document.getElementById("pwd1").value+"<br>";
		if (document.getElementById("pwd1").value === document.getElementById("pwd2").value){
			s+="Valido";
		}else{
			s+="Invalido, los campos no coinciden<br>"
		}
	}else{
		s = "<br>No hay password de entrada<br>Invalido";
	}
	document.getElementById("l4p1").innerHTML = s;
}

document.getElementById("botonValidar").onclick = validar;


function l4p2 () {
	var cantPizza = (document.getElementById("cantPizza").value * 80);
	var cantHambu = (document.getElementById("cantHambu").value * 50);
	var cantTaco = (document.getElementById("cantTaco").value * 10);
	var total = cantPizza + cantHambu + cantTaco;
	var iva = total * 0.16;
	var s ="<br>BIENVENIDO!<br>Pizzas: "+cantPizza+"<br>Hamburguesas: "+cantHambu+"<br>Tacos: "+cantTaco+"<br><br>";
	s+="SUBTOTAL: "+total+"<br>IVA(16%): "+iva.toFixed(2)+"<br>TOTAL: "+(iva+total).toFixed(2)+"<br>. <br>";

	document.getElementById("l4p2").innerHTML= s;
}

document.getElementById("opcProd").onchange = l4p2;

forma = document.getElementById("l4f1");



function l4p3(){
	var s = "<ul>";

	var verdad = true;

	if(forma[0].value.length <= 7){
		s+= "<li>La longitud del usuario debe ser mayor a 7 caracteres</li>";
		verdad = false;
	}

	var em = forma[1].value;
	if(em.indexOf("@") == -1 && em.indexOf(".") == -1){
		s+= "<li>Formato invalido de correo</li>"
		verdad = false;
	}

	var pwd = forma[2].value;
	if(pwd.length < 8){
		s+= "<li>La longitud de la contrasena debe ser mayor a 7 caracteres</li>";
		verdad = false;
	}
	if(!pwd.match(/[a-z]/)){
		s+= "<li>Contrasena no contiene minusculas</li>";
		verdad = false;
	}
	if(!pwd.match(/[A-Z]/)){
		s+= "<li>Contrasena no contiene mayuscula</li>";
		verdad = false;
	}
	if(!pwd.match(/[0-9]/)){
		s+= "<li>Contrasena no contiene numero</li>";
		verdad = false;
	}
	if(!pwd.match(/[!@#$%^&*)(+=._-]/)){
		s+= "<li>Contrasena no contiene caracter especial</li>";
		verdad = false;
	}
	if(pwd !== forma[3].value){
		s+="<li>Contrasenas no coinciden</li>";
		verdad = false;
	}
	if(forma[6].value < 18){
		s+="<li>Debe ser mayor de edad</li>";
		verdad = false;
	}
	if(forma[4].value === ""){
		s+="<li>Nombre es necesario</li>";
		verdad = false;
	}
	if(forma[5].value === ""){
		s+="<li>Apellido es necesario</li>";
		verdad = false;
	}
	if(!(forma[7].checked || forma[8].checked)){
		s+="<li>Seleccione un genero</li>";
		verdad = false;
	}

	if(verdad){
		s="Registro exitoso "+forma[0].value+"<br>Un correo de confirmacion sera enviado a: "+forma[1].value;
	}


	s+="</ul>";
	document.getElementById("l4p3").innerHTML = s;
}


document.getElementById("l4b3").onclick = l4p3;


document.getElementById("l4p3").innerHTML = "<ul><li>Usuario mayor a 8 caracteres</li><li>Contrasena mayor a 8 caracteres</li><li>Contrasena debe incluir minusculas, mayusculas, numero y un caracter especial</li><li>Debe ser mayor de edad</li></ul>"

var aparecido = false;

function aparecer(){
	var marca = document.getElementById("marcadeagua");
    marca.style.webkitAnimation = 'none';
    marca.style.opacity ="0";
    setTimeout(function(){marca.style.webkitAnimation = ''},1);
	marca.style.visibility = "visible";
	aparecido = true;
}

function checar(){
	if(window.scrollY < 150){
		document.getElementById("marcadeagua").style.visibility = "hidden";
		aparecido = false;
	}
	if(window.scrollY > 150 && !aparecido){
		aparecer();
	}
}

window.onscroll = checar;

document.getElementById("nav").style["margin-top"] = document.getElementById("header").offsetHeight+"px";
document.getElementById("saludo").style["margin-top"] = document.getElementById("header").offsetHeight+"px";
document.getElementById("cuerpo").style["margin-top"]=(40+document.getElementById("txtHeader").offsetHeight+document.getElementById("nav").offsetHeight)+"px";

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}