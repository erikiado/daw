<?php

include_once("model.php");


if(isset($_POST['l10_submit'])){
	$em1 = $_POST['email1'];
	$pw1 = $_POST['password1'];
	$pw2 = $_POST['password2'];
	$uname = $_POST['uname'];
	$valido = True;

	session_start();
	$_SESSION['u'] = $uname;

	$errores = "<ul class='center'>"; 
	$sHtml = '';


	if(empty($em1) or empty($pw1) or empty($pw2) or empty($uname)){
		$valido = False;
		$errores .= "<li>Se deben completar todos los campos</li>";
	}


	if(!preg_match("/^[a-zA-Z0-9]*$/",$uname)){
		$valido = False;
		$errores .= "<li>El nombre debe contener solo letras y numeros</li>";
	}

	if(!filter_var($em1, FILTER_VALIDATE_EMAIL)){
		$valido = False;
		$errores .= "<li>El email no tiene un formato valido</li>";
	}

	if(strlen($pw1) < 8){
		$valido = False;
		$errores .= "<li>La contrasena debe ser mayo a 8 caracteres</li>";
	}


	if(!($pw1 === $pw2)){
		$valido = False;
		$errores .= "<li>Las contrasenas no coinciden</li>";
	}


	$errores .= "</ul>";

	if($valido){
		include("respuesta.html");
	}else{
		include("error.html");
	}
	
} 
		include("respuesta.html");



?>