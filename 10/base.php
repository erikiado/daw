<html>
    <head>
        <?php
            include_once("model.php");

                $u = $_POST['queUsuario'];
                $tabla = buscarUsuario($u);
        
            
        ?>  

        <meta charset="UTF-8">
        <title>Laboratorio 10 - Respuesta</title>

        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <!--Iconos-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link type="text/css" rel="stylesheet" href="css/mi.css"/>      
    </head>

    <body>

    <header>
        <nav class="top-nav blue-grey darken-2">
            <div class="container">
                <div class="nav-wrapper">
                    <a href="#!" class="brand-logo">Lab 10</a>
                    
                    <ul class="right hide-on-med-and-down">
                        <li><a href="https://www.google.com"><i class="material-icons">search</i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    
    <main>
        
         <br><br>

         <h2 class="center">Usuarios:</h2>
         <br>
         <div class="container">
            <div class="row">
                <div class="col s12">
                  <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <br>
                        <div class="container">
                        
                        <?php echo $tabla?>

                        </div>
                    </div>
                    <div class="card-action right">
                      <a href="guion.php">Regresar</a>
                    </div>
                  </div>
                </div>
              </div>
         </div>         

    <br><br><br><br>
    </main>

    <footer class="page-footer blue-grey darken-2">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer</h5>
                <p class="grey-text text-lighten-4">About</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Enlace</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            2015 - Materialize 
            <a class="grey-text text-lighten-4 right" href="http://www.sublimetext.com/">Sublime Text</a>
            </div>
          </div>
        </footer>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/guion.js"></script>


    </body>
    
</html>
