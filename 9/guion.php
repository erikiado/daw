<?php

function ocultarPass($pass){
	$newPass = substr($pass, 0,1);
	$newPass .= str_repeat("*", (strlen($pass)-2));
	$newPass .= substr($pass, -1);
	return $newPass;
}

if(isset($_POST['logout'])){
	session_start();
	session_destroy();
	include("index.html");
}

if(isset($_POST['l9_submit'])){
	$first = $_POST['first_name'];
	$last = $_POST['last_name'];
	$em1 = $_POST['email1'];
	$em2 = $_POST['email2'];
	$pw1 = $_POST['password1'];
	$pw2 = $_POST['password2'];
	$bd = $_POST['cumple'];
	$dirArch = "img/";
	$foto = $dirArch . basename($_FILES['archivo']['name']);
	$uname = $_POST['uname'];
	$valido = True;
	session_start();
	$_SESSION['uname'] = $uname;
	$_SESSION['avatar'] = $foto;

	$errores = "<ul class='center'>"; 
	$sHtml = '';


	if(empty($first) or empty($last) or empty($em1) or empty($em2) or empty($pw1) or empty($pw2) or empty($bd) or empty($uname)){
		$valido = False;
		$errores .= "<li>Se deben completar todos los campos</li>";
	}

	if (!move_uploaded_file($_FILES['archivo']['tmp_name'], $foto)) {
		$valido = False;
		$errores .= "<li>Archivo no valido</li>";
	}

	if(!preg_match("/^[a-zA-Z ]*$/",$first)){
		$valido = False;
		$errores .= "<li>El nombre debe contener solo letras y espacios</li>";
	}

	if(!preg_match("/^[a-zA-Z ]*$/",$last)){
		$valido = False;
		$errores .= "<li>El apellido debe contener solo letras y espacios</li>";
	}

	if(!preg_match("/^[a-zA-Z0-9]*$/",$uname)){
		$valido = False;
		$errores .= "<li>El nombre debe contener solo letras y numeros</li>";
	}

	if(!filter_var($em1, FILTER_VALIDATE_EMAIL)){
		$valido = False;
		$errores .= "<li>El email no tiene un formato valido</li>";
	}

	if(strlen($pw1) < 8){
		$valido = False;
		$errores .= "<li>La contrasena debe ser mayo a 8 caracteres</li>";
	}

	if(!($em1 === $em2)){
		$valido = False;
		$errores .= "<li>Los correos no coinciden</li>";
	}

	if(!($pw1 === $pw2)){
		$valido = False;
		$errores .= "<li>Las contrasenas no coinciden</li>";
	}


	$errores .= "</ul>";

	if($valido){
		include("respuesta.html");
	}else{
		include("error.html");
	}
	
} 


?>